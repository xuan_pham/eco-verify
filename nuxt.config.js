require('dotenv').config();
import https from 'https';
const httpsAgent = new https.Agent({
	rejectUnauthorized: false
});
const { CI_PAGES_URL } = process.env;
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname;

module.exports = {
	ssr: false,

	/*
	 ** Headers of the page
	 */
	head: {
		title: 'ECO+ Share profile',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
			{ 'http-equiv': 'ScreenOrientation', content: 'autoRotate:disabled' },
			{ name: 'theme-color', content: '#ffffff' },
			{ hid: 'description', name: 'description', content: 'ECO PLUS' },
			{
				hid: 'image',
				property: 'image',
				content: 'https://ecopharma.com.vn/system/cms/themes/default/img/logo.png'
			}
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/faviconvt.ico' }],
		htmlAttrs: {
			lang: 'vi-VN'
		}
	},
	pageTransition: {
		name: 'layout',
		mode: 'out-in',
		beforeEnter(el) {
			//console.log('Before enter...');
		}
	},

	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: '#100fff' },

  generate: {
    dir: 'public'
  },

	/*
	 ** Global CSS
	 */
	css: ['@/assets/css/custom.scss'],

	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: [
		'@/plugins/axios',
		'@/plugins/async-computed',
		'@/plugins/composition-api',
		'@/plugins/globals',
		'@/plugins/currency-filter',
		'@/plugins/lodash'
	],
	router: {
		middleware: 'authenticated',
    base
	},
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://github.com/nuxt-community/axios-module#usage
		'@nuxtjs/axios',
		// Doc: https://bootstrap-vue.js.org/docs/
		'bootstrap-vue/nuxt',
		'@nuxtjs/pwa',
		'vue-social-sharing/nuxt',
		'@nuxtjs/sentry'
	],
	pwa: {
		meta: {
			theme_color: '#ffffff'
		},
		manifest: {
			name: 'ECO Share Profile',
			short_name: 'ECO Share Profile',
			lang: 'vi'
		}
	},
	buildModules: [
		// Simple usage
		'@nuxtjs/dotenv',
		'@nuxtjs/moment'
	],
	bootstrapVue: {
		bootstrapCSS: false, // Or `css: false`
		bootstrapVueCSS: false, // Or `bvCSS: false`
		icons: true
	},
	/*
	 ** Axios module configuration
	 */
	axios: {
		proxy: true
	},

	proxy: {
		'/api/': process.env.API_ENDPOINT
	},

	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {
			config.module.rules.push({
				test: /\.(ogg|mp3|wav|mpe?g)$/i,
				loader: 'file-loader',
				options: {
					name: '[path][name].[ext]'
				}
			});
		}
	}
};
