export const strict = false;
export const state = () => ({
	shareProfiles: null,
	requestInfo: null,
	token: null
});

export const mutations = {
	SET_SHARE_PROFILES(state, data) {
		state.shareProfiles = data;
	},
	SET_REQUEST_INFO(state, data) {
		state.requestInfo = data;
	}
};

export const actions = {};
